﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour 
{
	private static MainCamera staticInstance;
	public static MainCamera Instance
	{
		get 
		{ 
			if (staticInstance == null)
				staticInstance = GameObject.FindObjectOfType<MainCamera>();
			return staticInstance;
		}
	}

	public Vector3 BottomLeftCameraPoint
	{
		get { return bottomLeftCameraPoint; }
	}

	public Vector2 WorldDimentions
	{
		get { return worldDimentions; }
	}

	public Vector3 WorldCenter
	{
		get { return worldCenter; }
	}

	private Vector3 bottomLeftCameraPoint;
	private Vector3 worldCenter;
	private Vector2 worldDimentions;

	void Awake() 
	{
		bottomLeftCameraPoint = Camera.main.ViewportToWorldPoint(Vector2.zero);
		worldCenter = Camera.main.ViewportToWorldPoint(Vector3.one * 0.5f);

		Vector3 topRightCorner = Camera.main.ViewportToWorldPoint(Vector2.one);

		bottomLeftCameraPoint.z = topRightCorner.z = worldCenter.z = 0;

		worldDimentions = new Vector2(Mathf.Abs(bottomLeftCameraPoint.x - topRightCorner.x), Mathf.Abs(bottomLeftCameraPoint.y - topRightCorner.y));
	}
}
