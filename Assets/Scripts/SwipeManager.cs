﻿using UnityEngine;

public class SwipeManager : MonoBehaviour
{
	public System.Action<Vector3, Vector3> OnSwipe;
	public System.Action<Vector3, bool> OnTouchPressed;

	public float minSwipeLength = 5f;
	Vector3 firstPressPos;
	Vector3 secondPressPos;
	Vector2 currentSwipe;
	
	Vector3 firstClickPos;
	Vector3 secondClickPos;

	void Update()
	{
		DetectSwipe();
	}
	
	public void DetectSwipe()
	{
		if (Input.touches.Length > 0)
		{
			Touch t = Input.GetTouch(0);
			if (t.phase == TouchPhase.Began)
			{
				firstPressPos = new Vector3(t.position.x, t.position.y);
				OnTouchPressed(World(firstPressPos), true);
			}
			
			if (t.phase == TouchPhase.Ended)
			{
				secondPressPos = new Vector3(t.position.x, t.position.y);
				currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
				
				// Make sure it was a legit swipe, not a tap
				if (currentSwipe.magnitude < minSwipeLength)
				{
					OnTouchPressed(World(firstPressPos), false);
					return;
				}

				NotifySubscribers(firstPressPos, secondPressPos);
			}
		} 
		else 
		{
			if (Input.GetMouseButtonDown(0))
			{
				firstClickPos = Input.mousePosition;
				OnTouchPressed(World(firstClickPos), true);
			} 

			if (Input.GetMouseButtonUp(0))
			{
				secondClickPos = Input.mousePosition;
				currentSwipe = new Vector2(secondClickPos.x - firstClickPos.x, secondClickPos.y - firstClickPos.y);
				
				// Make sure it was a legit swipe, not a tap
				if (currentSwipe.magnitude < minSwipeLength)
				{
					OnTouchPressed(World(firstClickPos), false);
					return;
				}

				NotifySubscribers(firstClickPos, secondClickPos);
			}
		}
	}

	public Vector3 World(Vector3 screen)
	{
		return Camera.main.ScreenToWorldPoint(screen);
	}

	void NotifySubscribers(Vector3 startSwipePos, Vector3 endSwipePos)
	{
		OnTouchPressed(World(firstClickPos), false);
		if (null != OnSwipe)
		{
			Vector3 hitPoint1 = World(startSwipePos);
			Vector3 hitPoint2 = World(endSwipePos);

			OnSwipe(hitPoint1, hitPoint2);
		}
	}
}