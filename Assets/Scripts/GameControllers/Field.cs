﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Field : MonoBehaviour 
{
	public Tile[,] Tiles;
	public Item[,] Items;

	public Transform GameTilesRoot;
	public List<Swap> possibleSwaps;

	public int Width = 10;
	public int Height = 10;

	private Vector3 fieldOffset;

	static Field staticInstance;
	public static Field Instance
	{
		get 
		{
			if (staticInstance == null)
				staticInstance = GameObject.FindObjectOfType<Field>();
			return staticInstance;
		}
	}
	
	public Vector3 GameToWorld(IntVector position)
	{
		return new Vector3(position.X, position.Y, 0) + fieldOffset;
	}

	public IntVector WorldToGame(Vector3 position)
	{
		Vector3 shiftedPos = position - fieldOffset;
		return new IntVector(Mathf.RoundToInt(shiftedPos.x), Mathf.RoundToInt(shiftedPos.y));
	}

	void Awake () 
	{
		possibleSwaps = new List<Swap>();
		Tiles = new Tile[Width, Height];
		Items = new Item[Width, Height];

		fieldOffset = MainCamera.Instance.WorldCenter - new Vector3(Width * 0.5f, Height * 0.5f, 0) + new Vector3(0.5f, 0.5f, 0.0f);
	}

	public void ClearField()
	{
		for (int x = 0; x < Width; ++x)
			for (int y = 0; y < Height; ++y)
		{
			UnityEngine.Object.Destroy(Items[x, y].gameObject);
			Items[x, y] = null;
		}
	}

	public Item GetItem(Vector3 position)
	{
		IntVector gamePosition = WorldToGame(position);
		if (IsValid(gamePosition))
			return GetItem(gamePosition);
		return null;
	}

	public Item GetItem(IntVector position)
	{
		return Items[position.X, position.Y];
	}

	public void SetItem(IntVector position, Item item)
	{
		Items[position.X, position.Y] = item;
	}

	public bool IsValid(IntVector position)
	{
		return position.X < Width && position.Y < Height && position.X >= 0 && position.Y >= 0;
	}

	public void GenerateTiles()
	{
		for (int x = 0; x < Width; ++x)
			for (int y = 0; y < Height; ++y)
		{
			var tile = (GameObject.Instantiate(Resources.Load("Prefabs/Tile")) as GameObject).GetComponent<Tile>();
			tile.Position = new IntVector(x, y);
			tile.transform.parent = GameTilesRoot;
			Tiles[x, y] = tile;
		}
	}

	public Item CreateItemAt(IntVector position, EItemType type)
	{
		string itemPrefabName = "Prefabs/Item";
		var itemObject = (GameObject.Instantiate(Resources.Load(itemPrefabName)) as GameObject).GetComponent<Item>();
		Items[position.X, position.Y] = itemObject;
		Items[position.X, position.Y].Tile = Tiles[position.X, position.Y];
		
		itemObject.transform.parent = GameTilesRoot;
		itemObject.transform.position = Items[position.X, position.Y].Tile.transform.position + Vector3.back;
		Items[position.X, position.Y].ItemType = type;

		return itemObject;
	}

	public bool HasChainAt(IntVector position)
	{
		EItemType itemType = Items[position.X, position.Y].ItemType;
		
		int horzLength = 1;
		for (int i = position.Y - 1; i >= 0 && Items[position.X, i].ItemType == itemType; i--, horzLength++) ;
		for (int i = position.Y + 1; i < Height && Items[position.X, i].ItemType == itemType; i++, horzLength++) ;
		if (horzLength >= 3) return true;
		
		int vertLength = 1;
		for (int i = position.X - 1; i >= 0 && Items[i, position.Y].ItemType == itemType; i--, vertLength++) ;
		for (int i = position.X + 1; i < Width && Items[i, position.Y].ItemType == itemType; i++, vertLength++) ;
		return (vertLength >= 3);
	}

	public void DetectPossibleSwaps()
	{
		for (int x = 0; x < Width; x++) 
		{
			for (int y = 0; y < Height; y++)
			{
				Item item = Items[x, y];
				if (item != null)
				{
					if (y < Height - 1)
					{
						IntVector start = new IntVector(x, y + 1);
						IntVector end = new IntVector(x, y);
						AddSwapIfPossible(start, end);
					}
					if (x < Width - 1)
					{
						IntVector start = new IntVector(x + 1, y);
						IntVector end = new IntVector(x, y);
						AddSwapIfPossible(start, end);
					}
				}
			}
		}
	}

	public void AddSwapIfPossible(IntVector start, IntVector end)
	{
		Item other = Items[start.X, start.Y];
		Item item = Items[end.X, end.Y];
		if (other != null) 
		{
			Items[end.X, end.Y] = other;
			Items[start.X, start.Y] = item;
			
			if (HasChainAt(start) || HasChainAt(end))
			{
				Swap swap = new Swap(item, other);
				if (!possibleSwaps.Contains(swap))
					possibleSwaps.Add(swap);
			}
			Items[end.X, end.Y] = item;
			Items[start.X, start.Y] = other;
		}
	}

	public List<Chain> DetectMatches(bool horizontal)
	{
		List<Chain> chainList = new List<Chain>();

		System.Func<int,int,Item> getItem = (x, y) => 
		{
			return horizontal ? Items[y,x] : Items[x,y];
		};

		int firstLimit = horizontal ? Height : Width;
		int secondLimit = horizontal ? Width - 2 : Height - 2;
		
		for (int x = 0; x < firstLimit; x++)
		{
			for (int y = 0; y < secondLimit;)
			{
				if (getItem(x,y) != null)
				{
					EItemType matchType = getItem(x, y).ItemType;

					if (getItem(x, y + 1).ItemType == matchType
					    && getItem(x, y + 2).ItemType == matchType)
					{
						Chain chain = new Chain();
						do
						{
							chain.AddItem(getItem(x, y));
							y++;
						}
						while (y < secondLimit + 2 && getItem(x, y).ItemType == matchType);

						chainList.Add(chain);
						continue;
					}
				}
				y++;
			}
		}
		return chainList;
	} 
}
