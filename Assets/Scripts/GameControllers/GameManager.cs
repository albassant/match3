﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
	public UILabel ScoreLabel;
	public UITexture FadeTexture;

	private GameLogic game;
	private SwipeManager swipeController;

	private int comboMultiplier = 1;
	
	void Start()
	{
		swipeController = GameObject.FindObjectOfType<SwipeManager>() as SwipeManager;
		swipeController.OnSwipe += HandleCustomInput;
		swipeController.OnTouchPressed += HandleTouchChange;

		game = this.gameObject.GetComponent<GameLogic>();
	}

	void Update()
	{
		ScoreLabel.text = game.Score.ToString();
	}
	
	void HandleCustomInput(Vector3 start, Vector3 end)
	{
		StartCoroutine(game.TrySwapAction(start, end));
	}

	void HandleTouchChange(Vector3 position, bool pressed)
	{
		game.TrySelectItem(position, pressed);
	}

	void CalculateScores(List<Chain> chains)
	{
		foreach (Chain chain in chains)
		{
			chain.Score = 60 * (chain.GetItems().Count - 2) * comboMultiplier;
			comboMultiplier++;
		}
	}

	public void RestartButtonClicked()
	{
		StartCoroutine(RestartCoroutine());
	}

	IEnumerator RestartCoroutine()
	{
		FadeTexture.gameObject.SetActive(true);
		FadeTexture.alpha = 0.0f;
		yield return DOTween.To(() => FadeTexture.alpha, (a) => FadeTexture.alpha = a, 1.0f, 0.6f).WaitForCompletion();

		game.RestartGame();

		yield return DOTween.To(() => FadeTexture.alpha, (a) => FadeTexture.alpha = a, 0.0f, 0.6f).WaitForCompletion();
		FadeTexture.gameObject.SetActive(false);
	}
	
	void OnDestroy()
	{
		swipeController.OnSwipe -= HandleCustomInput;
		swipeController.OnTouchPressed -= HandleTouchChange;	
	}
}
