﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameLogic : MonoBehaviour
{	
	private Field field;
	private bool blockInput = false;

	public int Score
	{
		get; set;
	}
	
	private void Start()
	{
		field = Field.Instance;
		field.GenerateTiles();
		Shuffle();
	}

	public void RestartGame()
	{
		Score = 0;
		field.ClearField();
		Shuffle();

		this.StopAllCoroutines();
		blockInput = false;
	}

	public List<Item> Shuffle()
	{
		List<Item> itemsList = new List<Item>();
		do
		{
			itemsList = GenerateItems();
			field.DetectPossibleSwaps();
		}
		while (field.possibleSwaps.Count == 0);
		
		return itemsList;
	}

	private List<Item> GenerateItems()
	{
		List<Item> itemsList = new List<Item>();

		for (int x = 0; x < field.Width; ++x)
			for (int y = 0; y < field.Height; ++y)
			{
				EItemType itemType = EItemType.Type1;
				do 
				{
					itemType = Item.GenerateRandomType();
				}
				// make sure we won't generate chains that are ready for removal 
				while ((y >= 2 && field.Items[x, y - 1].ItemType == itemType && field.Items[x, y - 2].ItemType == itemType)
			       || (x >= 2 && field.Items[x - 1, y].ItemType == itemType && field.Items[x - 2, y].ItemType == itemType));

				var newItem = field.CreateItemAt(new IntVector(x,y), itemType);
				itemsList.Add(newItem);
			}
		return itemsList;
	}

	private IEnumerator AnimateSwap(Swap swap, System.Action swapCompleted = null)
	{
		Tile firstTile = swap.FirstItem.Tile;
		Tile secondTile = swap.SecondItem.Tile;

		field.SetItem(firstTile.Position, swap.SecondItem);
		swap.SecondItem.Tile = firstTile;

		field.SetItem(secondTile.Position, swap.FirstItem);
		swap.FirstItem.Tile = secondTile;

		StartCoroutine(swap.SecondItem.MatchPositionWithTile());
		yield return StartCoroutine(swap.FirstItem.MatchPositionWithTile());
	}

	public IEnumerator TrySwapAction(Vector3 worldStart, Vector3 worldEnd)
	{
		if (blockInput)
			yield break;

		blockInput = true;

		IntVector start = field.WorldToGame(worldStart);
		IntVector end = field.WorldToGame(worldEnd);

		if (!field.IsValid(start) || !field.IsValid(end) || (end - start).SqrMagnitude != 1)
		{
			blockInput = false;
			yield break;
		}

		Swap swap = new Swap(field.GetItem(start), field.GetItem(end));

		if (field.possibleSwaps.Contains(swap))
		{
			yield return StartCoroutine(AnimateSwap(swap));
			yield return StartCoroutine(RemovalAnimationLoop());
		}
		else 
		{
			yield return StartCoroutine(AnimateSwap(swap));
			var tmp = swap.FirstItem;
			swap.FirstItem = swap.SecondItem;
			swap.SecondItem = tmp;
			yield return StartCoroutine(AnimateSwap(swap));
		}

		blockInput = false;
	}

	public void TrySelectItem(Vector3 worldPos, bool select)
	{
		var item = field.GetItem(worldPos);
		if (item != null && select)
		{
			item.IsHighlighted = true;
		}
		if (!select)
		{
			foreach (var i in field.Items)
			{
				i.IsHighlighted = false;
			}
		}
	}

	private IEnumerator RemovalAnimationLoop()
	{
		List<Chain> chainsToRemove = GetChainsForRemoval();

		while (chainsToRemove.Count != 0)
		{
			yield return StartCoroutine(RemoveItems(chainsToRemove));

			var items = FillHoles();
			items.AddRange(GenerateMissingItems());

			foreach (var item in items)
			{
				StartCoroutine(item.MatchPositionWithTile());
			}
			while (items.FirstOrDefault((i) => i.IsAnimated) != null)
			{
				yield return null;
			}

			chainsToRemove = GetChainsForRemoval();
		}
		
		field.DetectPossibleSwaps();
	}

	List<Chain> GetChainsForRemoval()
	{
		List<Chain> horizontalChains = field.DetectMatches(true);
		List<Chain> verticalChains = field.DetectMatches(false);

		horizontalChains.AddRange(verticalChains);
		return horizontalChains;
	}

	IEnumerator RemoveItems(List<Chain> chains)
	{
		foreach (Chain chain in chains)
		{
			foreach (Item item in chain.GetItems())
			{
				StartCoroutine(item.PlayRemoveAnimation());
				field.SetItem(item.Tile.Position, null);
				Score += 10;
			}
		}

		while (chains.FirstOrDefault((c) => c.GetItems().FirstOrDefault(i => i.IsAnimated) != null) != null)
		{
			yield return null;
		}
	}

	List<Item> FillHoles()
	{
		List<Item> columns = new List<Item>();
		
		for (int x = 0; x < field.Width; x++)
		{
			for (int y = 0; y < field.Height; y++)
			{
				if (field.Items[x, y] == null)
				{
					for (int yUp = y + 1; yUp < field.Height; yUp++)
					{
						Item item = field.Items[x, yUp];
						if (item != null)
						{
							field.Items[x, y] = item;
							item.Tile = field.Tiles[x, y];
							field.Items[x, yUp] = null;
							columns.Add(item);
							break;
						}
					}
				}
			}
		}
		return columns;
	}

	List<Item> GenerateMissingItems()
	{
		List<Item> newItems = new List<Item>();
		EItemType itemType = EItemType.Type1;
		
		for (int x = 0; x < field.Width; x++)
		{
			List<Item> itemsFromThisColumn = new List<Item>();

			int y;
			for (y = field.Height - 1; y >= 0 && field.Items[x, y] == null; y--)
			{
				EItemType newItemType;
				do
				{
					newItemType = Item.GenerateRandomType();
				} 
				while (newItemType == itemType);

				itemType = newItemType;					
				var newItem = field.CreateItemAt(new IntVector(x,y), itemType);
				itemsFromThisColumn.Add(newItem);
				StartCoroutine(newItem.PlayAppearingAnimation());
			}

			foreach (var item in itemsFromThisColumn)
			{
				item.transform.position += Vector3.up * (field.Height - 1 - y);
			}

			newItems.AddRange(itemsFromThisColumn);
		}
		return newItems;
	}
}
