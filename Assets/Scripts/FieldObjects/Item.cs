using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System;

public class Swap : IEquatable<Swap>
{
	public Item FirstItem;
	public Item SecondItem;

	public Swap(Item firstItem, Item secondItem)
	{
		FirstItem = firstItem;
		SecondItem = secondItem;
	}
	
	public bool Equals(Swap another)
	{
		return this.FirstItem == another.FirstItem && this.SecondItem == another.SecondItem ||
			this.SecondItem == another.FirstItem && this.FirstItem == another.SecondItem;
	}
}

public enum EChainType
{
	Horizontal,
	Vertical,
}

public class Chain
{
	private List<Item> items = new List<Item>();
	
	public EChainType ChainType;
	
	public int Score;
	
	public void AddItem(Item newItem)
	{
		if (!items.Contains(newItem))
			items.Add(newItem);
	}
	
	public List<Item> GetItems()
	{
		return items;
	}
}

public enum EItemType : byte
{
	Type1 = 0,
	Type2,
	Type3,
	Type4,
	Type5,
	Type6,
}

public class Item : MonoBehaviour 
{
	public Texture[] ItemSkins;
	public AnimationCurve DestroyAnimation;

	public GameObject ItemHighlight;

	public Tile Tile;
	private EItemType itemType;

	[System.NonSerialized]
	public bool IsAnimated = false;

	private bool isHighlighted = false;
	public bool IsHighlighted
	{
		set 
		{
			isHighlighted = value;

			if (isHighlighted)
				StartCoroutine(SelectAnimation());
		}
		get { return isHighlighted; }
	}

	public EItemType ItemType
	{
		set 
		{
			itemType = value;
			this.UpdateSkin();
		}
		get { return itemType; }
	}

	public void UpdateSkin()
	{
		this.GetComponent<Renderer>().material.mainTexture = ItemSkins[(int)ItemType];
	}

	public virtual void Start()
	{
		this.UpdateSkin();
		this.transform.localScale = Vector3.one * 0.8f;
		ItemHighlight.SetActive(false);
	}

	public static EItemType GenerateRandomType()
	{
		return (EItemType)(byte)UnityEngine.Random.Range(0, 4);
	}

	public IEnumerator DeselectAnimation()
	{
		yield return null;
	}

	public IEnumerator SelectAnimation()
	{
		Vector3 endScale = Vector3.one * 1.44f;

		ItemHighlight.SetActive(true);
		ItemHighlight.transform.localScale = Vector3.one;

		yield return DOTween.To(() => ItemHighlight.transform.localScale, (s) => ItemHighlight.transform.localScale = s, endScale, 0.2f).WaitForCompletion();

		while (isHighlighted)
		{
			yield return DOTween.Shake(() => transform.eulerAngles,
			              (angles) => 
			              {
								transform.rotation = Quaternion.Euler(angles);
						  }, 0.3f, 30).WaitForCompletion();
		}

		yield return DOTween.To(() => ItemHighlight.transform.localScale, (s) => ItemHighlight.transform.localScale = s, Vector3.one, 0.2f).WaitForCompletion();
		ItemHighlight.SetActive(false);

		IsHighlighted = false;
	}

	public IEnumerator MatchPositionWithTile()
	{
		IsAnimated = true;

		var newPosition = Tile.transform.position + Vector3.back;
		yield return DOTween.To(() => transform.position, (p) => transform.position = p, newPosition, 0.3f).WaitForCompletion();

		IsAnimated = false;
	}

	public IEnumerator PlayAppearingAnimation()
	{
		GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0);
		yield return DOTween.To(() => GetComponent<Renderer>().material.color, (c) => GetComponent<Renderer>().material.color = c, new Color(1, 1, 1, 1), 0.3f).WaitForCompletion();
	}

	public IEnumerator PlayRemoveAnimation()
	{
		IsAnimated = true;

		ItemHighlight.SetActive(false);
		var sequence = DOTween.Sequence();

		sequence.Insert(0, DOTween.To(() => GetComponent<Renderer>().material.color, (c) => GetComponent<Renderer>().material.color = c, new Color(1, 1, 1, 0), 0.3f));
		sequence.Insert(0, DOTween.To(() => this.transform.localScale, (s) => this.transform.localScale = s, Vector2.zero, 0.3f)
			.SetEase(DestroyAnimation));

		yield return sequence.WaitForCompletion();

		GameObject.Destroy(this.gameObject);
		IsAnimated = false;
	}
}
