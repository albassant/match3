﻿using UnityEngine;
using System.Collections;

public class FieldObject : MonoBehaviour 
{
	private IntVector position;
	
	public IntVector Position
	{
		set 
		{
			position = value;
			this.transform.position = Field.Instance.GameToWorld(position);
		}
		get { return position; }
	}
}
