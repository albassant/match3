﻿using UnityEngine;
using System.Collections;

public class IntVector
{
	public int X;
	public int Y;

	public IntVector(int x, int y)
	{
		X = x;
		Y = y;
	}

	public static IntVector operator +(IntVector vector1, IntVector vector2)
	{
		return new IntVector(vector1.X + vector2.X, vector1.Y + vector2.Y);
	}

	public static IntVector operator -(IntVector vector1, IntVector vector2)
	{
		return new IntVector(vector1.X - vector2.X, vector1.Y - vector2.Y);
	}

	public bool Equals(IntVector vector)
	{
		if (vector == null)
		{
			return false;
		}
		return (vector.X == X) && (vector.Y == Y);
	}

	public int SqrMagnitude
	{
		get { return X * X + Y * Y; }
	}

	public Vector3 UnityVector
	{
		get { return new Vector3(X, 0, Y); }
	}
}
