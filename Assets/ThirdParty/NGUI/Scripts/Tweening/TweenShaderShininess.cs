using UnityEngine;
using System.Collections;

public class TweenShaderShininess : UITweener
 {
	public float from = 0f;
	public float to = 1f;
	public bool includeChildren = false;

	private float shininess
	{
		get
		{
			foreach (var rendChild in GetComponentsInChildren<Renderer>())
			{
				if (rendChild.material.HasProperty("_Shininess"))
				{
					return rendChild.material.GetFloat("_Shininess");
				}
			}

			return 0.0f;
		}
		set
		{
			if (includeChildren)
			{
				foreach (var rendChild in GetComponentsInChildren<Renderer>())
				{
					foreach (var material in rendChild.materials)
						material.SetFloat("_Shininess", value);
				}

			}
			else
			{
				if (null != GetComponent<Renderer>())
				{
					GetComponent<Renderer>().material.SetFloat("_Shininess", value);
				}
			}
		}
	}

	override protected void OnUpdate (float factor, bool isFinished) 
	{ 
		shininess = Mathf.Lerp(from, to, factor); 
	}
	
	static public TweenShaderShininess Begin (GameObject go, float duration, float intensity)
	{
		TweenShaderShininess comp = UITweener.Begin<TweenShaderShininess>(go, duration);
		comp.from = comp.shininess;
		comp.to = intensity;
		
		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}
